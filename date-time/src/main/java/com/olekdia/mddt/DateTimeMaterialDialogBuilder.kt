package com.olekdia.mddt

import android.content.Context
import androidx.annotation.UiThread
import com.olekdia.common.extensions.ifNotNullAnd
import com.olekdia.datetimepicker.PickerBuilder
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.NOT_SET
import com.olekdia.datetimepicker.interfaces.DtChangeListener
import com.olekdia.datetimepicker.interfaces.HeaderSelectionListener
import com.olekdia.materialdialog.MaterialDialogBuilder

class DateTimeMaterialDialogBuilder(
    context: Context
) : BaseDateTimeMaterialDialogBuilder<DateTimeMaterialDialogBuilder, PickerBuilder>(context) {

    override val self: DateTimeMaterialDialogBuilder
        get() = this

    override val dtPickerBuilder: PickerBuilder = PickerBuilder(context)

    internal var dtCallback: DateTimeMaterialDialog.DtCallback? = null
        private set

    @JvmField
    internal var dateTitle: String? = null
    @JvmField
    internal var timeTitle: String? = null

    override fun customView(layoutRes: Int, wrapInScrollView: Boolean): MaterialDialogBuilder {
        throw IllegalStateException("You cannot use customView() with date time picker.")
    }

    // selected

    fun selectedDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): DateTimeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectedDate(year, month, day)
    }

    fun selectedTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectedTime(hour, minute, second)
    }

    fun selectedDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectedDateTime(year, month, day, hour, minute, second)
    }

    fun selectedDt(dt: DateTime): DateTimeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectedDt(dt)
    }

    // selectable

    fun selectableDate(vararg dates: Triple<Int, Int, Int>): DateTimeMaterialDialogBuilder =
        this.apply {
            dtPickerBuilder.selectableDate(*dates)
        }

    fun selectableTime(vararg times: Triple<Int, Int, Int>): DateTimeMaterialDialogBuilder =
        this.apply {
            dtPickerBuilder.selectableTime(*times)
        }

    fun selectableDt(selectableDt: Array<DateTime>?): DateTimeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectableDt(selectableDt)
    }

    // min

    fun minDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): DateTimeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.minDate(year, month, day)
    }

    fun minTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.minTime(hour, minute, second)
    }

    fun minDt(minDt: DateTime?): DateTimeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.minDt(minDt)
    }

    // max

    fun maxDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): DateTimeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.maxDate(year, month, day)
    }

    fun maxTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.maxTime(hour, minute, second)
    }

    fun maxDt(maxDt: DateTime?): DateTimeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.maxDt(maxDt)
    }

    fun dtCallback(
        dtCallback: DateTimeMaterialDialog.DtCallback
    ): DateTimeMaterialDialogBuilder = this.apply {
        this.dtCallback = dtCallback
    }

    // title

    fun dateTitle(dateTitle: String): DateTimeMaterialDialogBuilder = this.apply {
        this.dateTitle = dateTitle
    }

    fun timeTitle(timeTitle: String): DateTimeMaterialDialogBuilder = this.apply {
        this.timeTitle = timeTitle
    }

    @UiThread
    override fun build(): DateTimeMaterialDialog {
        invalidateColors()

        var dialog: DateTimeMaterialDialog? = null

        dtPickerBuilder.dtChangeListener(object : DtChangeListener {
            override fun onDateChanged(
                year: Int, month: Int, day: Int,
                hour: Int, minute: Int, second: Int
            ) {
                ifNotNullAnd(
                    dtCallback,
                    alwaysCallDtCallback
                ) { callback ->
                    callback.onSelection(
                        dialog,
                        year, month, day,
                        hour, minute, second
                    )
                }
            }
        })

        dtPickerBuilder.headerSelectionListener(object : HeaderSelectionListener {
            override fun onHeaderSelection(index: Int) {
                dialog?.setCorrespondingTitle(index)
            }
        })

        super.customView(dtPickerBuilder.build(), false)

        // This is workaround to have visible title at all
        if (title == null
            && (dateTitle != null
                || timeTitle != null)
        ) {
            title = ""
        }

        dialog = DateTimeMaterialDialog(this)

        dialog.setCorrespondingTitle()

        return dialog
    }
}