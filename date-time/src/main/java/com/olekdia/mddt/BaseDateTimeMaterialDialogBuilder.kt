package com.olekdia.mddt

import android.content.Context
import androidx.annotation.ColorInt
import androidx.annotation.IntRange
import com.olekdia.common.NumeralSystem
import com.olekdia.datetimepicker.BasePickerBuilder
import com.olekdia.materialdialog.MaterialDialogBuilder
import java.util.*

abstract class BaseDateTimeMaterialDialogBuilder
<T : BaseDateTimeMaterialDialogBuilder<T, P>, P : BasePickerBuilder<P>>(
    context: Context
) : MaterialDialogBuilder(context) {

    abstract val self: T

    protected abstract val dtPickerBuilder: BasePickerBuilder<P>

    protected var alwaysCallDtCallback: Boolean = false

    fun currentItem(currentItem: Int): T = self.apply {
        dtPickerBuilder.currentItem(currentItem)
    }

    override fun numeralSystem(@NumeralSystem numeralSystem: Int): T = self.apply {
        super.numeralSystem(numeralSystem)
        dtPickerBuilder.numeralSystem(numeralSystem)
    }

    fun is24HourMode(is24HourMode: Boolean): T = self.apply {
        dtPickerBuilder.is24HourMode(is24HourMode)
    }

    fun monthsOfYearLong(monthsOfYearLong: Array<String>): T = self.apply {
        dtPickerBuilder.monthsOfYearLong(monthsOfYearLong)
    }

    fun monthsOfYearShort(monthsOfYearShort: Array<String>): T = self.apply {
        dtPickerBuilder.monthsOfYearShort(monthsOfYearShort)
    }

    fun amPmText(amText: String, pmText: String): T = self.apply {
        dtPickerBuilder.amPmText(amText, pmText)
    }

    override fun isDarkTheme(isDarkTheme: Boolean): T = self.apply {
        super.isDarkTheme(isDarkTheme)
        dtPickerBuilder.isDarkTheme(isDarkTheme)
    }

    override fun widgetColor(@ColorInt color: Int): T = self.apply {
        super.widgetColor(color)
        dtPickerBuilder.accentColor(color)
    }

    override fun backgroundColor(color: Int): T = self.apply {
        super.backgroundColor(color)
        dtPickerBuilder.secondaryBackgroundColor(color)
    }

    fun headerBackgroundColor(headerBackgroundColor: Int): T = self.apply {
        dtPickerBuilder.primaryBackgroundColor(headerBackgroundColor)
    }

    override fun titleColor(@ColorInt color: Int): T = self.apply {
        super.titleColor(color)
        dtPickerBuilder.primaryTextColor(color)
    }

    override fun contentColor(@ColorInt color: Int): T = self.apply {
        super.contentColor(color)
        dtPickerBuilder.secondaryTextColor(color)
    }

    fun disabledTextColor(disabledTextColor: Int): T = self.apply {
        dtPickerBuilder.disabledTextColor(disabledTextColor)
    }

    fun highlightedTextColor(highlightedTextColor: Int): T = self.apply {
        dtPickerBuilder.highlightedTextColor(highlightedTextColor)
    }

    fun vibrate(vibrate: Boolean): T = self.apply {
        dtPickerBuilder.vibrate(vibrate)
    }

    fun daysOfWeek(daysOfWeek: Array<String>): T = self.apply {
        dtPickerBuilder.daysOfWeek(daysOfWeek)
    }

    fun firstDayOfWeek(
        @IntRange(
            from = Calendar.SUNDAY.toLong(),
            to = Calendar.SATURDAY.toLong()
        ) firstDayOfWeek: Int
    ): T = self.apply {
        dtPickerBuilder.firstDayOfWeek(firstDayOfWeek)
    }

    fun allowAutoAdvance(allowAutoAdvance: Boolean): T = self.apply {
        dtPickerBuilder.allowAutoAdvance(allowAutoAdvance)
    }

    fun alwaysCallDtCallback(): T = self.apply {
        alwaysCallDtCallback = true
    }
}