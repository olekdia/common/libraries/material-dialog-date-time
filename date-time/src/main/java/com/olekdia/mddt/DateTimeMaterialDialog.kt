package com.olekdia.mddt

import android.os.Bundle
import com.olekdia.common.extensions.ifNotNull
import com.olekdia.datetimepicker.Picker
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.PickerIndex.Companion.DAY
import com.olekdia.datetimepicker.common.PickerIndex.Companion.SECOND
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import com.olekdia.materialdialog.MaterialDialog

class DateTimeMaterialDialog(
    private val dtBuilder: DateTimeMaterialDialogBuilder
) : MaterialDialog(dtBuilder) {

    val dtPicker: Picker
        get() = dtBuilder.customView as Picker

    private val currItem: Int
        get() = dtPicker.currentItem

    val selectedDt: DateTime?
        get() = dtPicker.getSelectedDt()

    override fun onPositiveClick() {
        ifNotNull(
            dtBuilder.dtCallback,
            selectedDt
        ) { callback, dt ->
            callback.onSelection(
                this,
                dt.year, dt.month, dt.day,
                dt.hour24, dt.minute, dt.second
            )
        }
        super.onPositiveClick()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        setCorrespondingTitle(currItem)
    }

    internal fun setCorrespondingTitle(
        index: Int = currItem
    ) {
        if (!dtBuilder.title.isNullOrEmpty()) {
            return
        }

        if (index !in YEAR..SECOND) {
            setTitle("")
            return
        }

        setTitle(
            if (index in YEAR..DAY) {
                dtBuilder.dateTitle ?: ""
            } else {
                dtBuilder.timeTitle ?: ""
            }
        )
    }

    interface DtCallback {
        fun onSelection(
            dialog: MaterialDialog?,
            year: Int, month: Int, day: Int,
            hour: Int, minute: Int, second: Int
        )
    }
}