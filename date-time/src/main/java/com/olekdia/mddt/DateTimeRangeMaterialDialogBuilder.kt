package com.olekdia.mddt

import android.content.Context
import androidx.annotation.UiThread
import com.olekdia.common.extensions.ifNotNullAnd
import com.olekdia.datetimepicker.PickerRangeBuilder
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.NOT_SET
import com.olekdia.datetimepicker.interfaces.DtRangeChangeListener
import com.olekdia.datetimepicker.interfaces.HeaderRangeSelectionListener
import com.olekdia.materialdialog.MaterialDialogBuilder

class DateTimeRangeMaterialDialogBuilder(
    context: Context
) : BaseDateTimeMaterialDialogBuilder<DateTimeRangeMaterialDialogBuilder, PickerRangeBuilder>(
    context
) {

    override val self: DateTimeRangeMaterialDialogBuilder
        get() = this

    override val dtPickerBuilder: PickerRangeBuilder = PickerRangeBuilder(context)

    internal var dtCallback: DateTimeRangeMaterialDialog.DtCallback? = null
        private set

    @JvmField
    internal var startDateTitle: String? = null
    @JvmField
    internal var startTimeTitle: String? = null

    @JvmField
    internal var endDateTitle: String? = null
    @JvmField
    internal var endTimeTitle: String? = null

    override fun customView(layoutRes: Int, wrapInScrollView: Boolean): MaterialDialogBuilder {
        throw IllegalStateException("You cannot use customView() with date time picker.")
    }

    // selected

    fun selectedStartDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectedStartDate(year, month, day)
    }

    fun selectedStartTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectedStartTime(hour, minute, second)
    }

    fun selectedStartDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectedStartDateTime(year, month, day, hour, minute, second)
    }

    fun selectedStartDt(dt: DateTime): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectedStartDt(dt)
    }

    fun selectedEndDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectedEndDate(year, month, day)
    }

    fun selectedEndTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectedEndTime(hour, minute, second)
    }

    fun selectedEndDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectedEndDateTime(year, month, day, hour, minute, second)
    }

    fun selectedEndDt(dt: DateTime): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectedEndDt(dt)
    }

    // range part

    fun currentRangePart(currentRangePart: Int): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.currentRangePart(currentRangePart)
    }

    //selectable

    fun selectableStartDate(
        vararg dates: Triple<Int, Int, Int>
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectableStartDate(*dates)
    }

    fun selectableStartTime(
        vararg times: Triple<Int, Int, Int>
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectableStartTime(*times)
    }

    fun selectableStartDt(
        selectableDt: Array<DateTime>?
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectableStartDt(selectableDt)
    }

    fun selectableEndDate(
        vararg dates: Triple<Int, Int, Int>
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectableEndDate(*dates)
    }

    fun selectableEndTime(
        vararg times: Triple<Int, Int, Int>
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectableEndTime(*times)
    }

    fun selectableEndDt(
        selectableDt: Array<DateTime>?
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.selectableEndDt(selectableDt)
    }

    //min

    fun minStartDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.minStartDate(year, month, day)
    }

    fun minStartTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.minStartTime(hour, minute, second)
    }

    fun minStartDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.minStartDateTime(year, month, day, hour, minute, second)
    }

    fun minStartDt(minDt: DateTime?): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.minStartDt(minDt)
    }

    fun minEndDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.minEndDate(year, month, day)
    }

    fun minEndTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.minEndTime(hour, minute, second)
    }

    fun minEndDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.minEndDateTime(year, month, day, hour, minute, second)
    }

    fun minEndDt(minDt: DateTime?): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.minEndDt(minDt)
    }

    //max

    fun maxStartDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.maxStartDate(year, month, day)
    }

    fun maxStartTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.maxStartTime(hour, minute, second)
    }

    fun maxStartDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.maxStartDateTime(year, month, day, hour, minute, second)
    }

    fun maxStartDt(maxDt: DateTime?): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.maxStartDt(maxDt)
    }

    fun maxEndDate(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.maxEndDate(year, month, day)
    }

    fun maxEndTime(
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.maxEndTime(hour, minute, second)
    }

    fun maxEndDateTime(
        year: Int = NOT_SET,
        month: Int = NOT_SET,
        day: Int = NOT_SET,
        hour: Int = NOT_SET,
        minute: Int = NOT_SET,
        second: Int = NOT_SET
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.maxEndDateTime(year, month, day, hour, minute, second)
    }

    fun maxEndDt(maxDt: DateTime?): DateTimeRangeMaterialDialogBuilder = this.apply {
        dtPickerBuilder.maxEndDt(maxDt)
    }

    fun dtCallback(
        dtCallback: DateTimeRangeMaterialDialog.DtCallback
    ): DateTimeRangeMaterialDialogBuilder = this.apply {
        this.dtCallback = dtCallback
    }

    // title

    fun startDateTitle(startDateTitle: String): DateTimeRangeMaterialDialogBuilder = this.apply {
        this.startDateTitle = startDateTitle
    }

    fun startTimeTitle(startTimeTitle: String): DateTimeRangeMaterialDialogBuilder = this.apply {
        this.startTimeTitle = startTimeTitle
    }

    fun endDateTitle(endDateTitle: String): DateTimeRangeMaterialDialogBuilder = this.apply {
        this.endDateTitle = endDateTitle
    }

    fun endTimeTitle(endTimeTitle: String): DateTimeRangeMaterialDialogBuilder = this.apply {
        this.endTimeTitle = endTimeTitle
    }

    @UiThread
    override fun build(): DateTimeRangeMaterialDialog {
        invalidateColors()

        var dialog: DateTimeRangeMaterialDialog? = null

        dtPickerBuilder.dtRangeChangeListener(object : DtRangeChangeListener {
            override fun onRangeDateChanged(
                startYear: Int, startMonth: Int, startDay: Int,
                startHour: Int, startMinute: Int, startSecond: Int,
                endYear: Int, endMonth: Int, endDay: Int,
                endHour: Int, endMinute: Int, endSecond: Int
            ) {
                ifNotNullAnd(
                    dtCallback,
                    alwaysCallDtCallback
                ) { callback ->
                    callback.onSelection(
                        dialog,
                        startYear, startMonth, startDay,
                        startHour, startMinute, startSecond,
                        endYear, endMonth, endDay,
                        endHour, endMinute, endSecond
                    )
                }
            }
        })

        dtPickerBuilder.headerRangeSelectionListener(object : HeaderRangeSelectionListener {
            override fun onHeaderSelection(index: Int, part: Int) {
                dialog?.setCorrespondingTitle(index, part)
            }
        })

        super.customView(dtPickerBuilder.build(), false)

        // This is workaround to have visible title at all
        if (title == null
            && (startDateTitle != null
                || startTimeTitle != null
                || endDateTitle != null
                || endTimeTitle != null)
        ) {
            title = ""
        }

        dialog = DateTimeRangeMaterialDialog(this)

        dialog.setCorrespondingTitle()

        return dialog
    }
}