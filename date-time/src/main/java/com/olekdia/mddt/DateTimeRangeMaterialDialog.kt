package com.olekdia.mddt

import android.os.Bundle
import com.olekdia.common.extensions.ifNotNull
import com.olekdia.datetimepicker.Picker
import com.olekdia.datetimepicker.common.DateTime
import com.olekdia.datetimepicker.common.PickerIndex.Companion.DAY
import com.olekdia.datetimepicker.common.PickerIndex.Companion.SECOND
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import com.olekdia.datetimepicker.common.RangePart.Companion.END
import com.olekdia.datetimepicker.common.RangePart.Companion.START
import com.olekdia.materialdialog.MaterialDialog

class DateTimeRangeMaterialDialog(
    private val dtBuilder: DateTimeRangeMaterialDialogBuilder
) : MaterialDialog(dtBuilder) {

    val dtPicker: Picker
        get() = dtBuilder.customView as Picker

    private val currItem: Int
        get() = dtPicker.currentItem
    private val currRangePart: Int
        get() = dtPicker.currentRangePart

    val selectedStartDt: DateTime?
        get() = dtPicker.getSelectedStartDt()
    val selectedEndDt: DateTime?
        get() = dtPicker.getSelectedEndDt()

    override fun onPositiveClick() {
        ifNotNull(
            dtBuilder.dtCallback,
            selectedStartDt,
            selectedEndDt
        ) { callback, startDt, endDt ->
            callback.onSelection(
                this,
                startDt.year, startDt.month, startDt.day,
                startDt.hour24, startDt.minute, startDt.second,
                endDt.year, endDt.month, endDt.day,
                endDt.hour24, endDt.minute, endDt.second
            )
        }
        super.onPositiveClick()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        setCorrespondingTitle(currItem, currRangePart)
    }

    internal fun setCorrespondingTitle(
        index: Int = currItem,
        part: Int = currRangePart
    ) {
        if (!dtBuilder.title.isNullOrEmpty()) {
            return
        }

        if (index !in YEAR..SECOND
            || part !in START..END
        ) {
            setTitle("")
            return
        }

        setTitle(
            if (index in YEAR..DAY) {
                if (part == START) dtBuilder.startDateTitle ?: "" else dtBuilder.endDateTitle ?: ""
            } else {
                if (part == START) dtBuilder.startTimeTitle ?: "" else dtBuilder.endTimeTitle ?: ""
            }
        )
    }

    interface DtCallback {
        fun onSelection(
            dialog: MaterialDialog?,
            startYear: Int, startMonth: Int, startDay: Int,
            startHour: Int, startMinute: Int, startSecond: Int,
            endYear: Int, endMonth: Int, endDay: Int,
            endHour: Int, endMinute: Int, endSecond: Int
        )
    }
}