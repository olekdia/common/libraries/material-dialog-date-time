package com.olekdia.mddt

import android.content.Context
import android.os.Build
import androidx.test.platform.app.InstrumentationRegistry
import com.olekdia.common.INVALID
import com.olekdia.datetimepicker.Picker
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode
import java.util.*

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
@LooperMode(LooperMode.Mode.LEGACY)
class DateTimeDialog_CreationTest {

    private lateinit var context: Context

    private lateinit var dialog: DateTimeMaterialDialog
    private lateinit var dialogRange: DateTimeRangeMaterialDialog

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
    }

    @Test
    fun `set selected date - dialog created, picker's values are valid`() {
        dialog = DateTimeMaterialDialogBuilder(context)
            .selectedDate(2013, Calendar.FEBRUARY, 1)
            .title("Choose date time")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build() as DateTimeMaterialDialog

        val picker: Picker = dialog.dtPicker

        val date: Triple<Int, Int, Int> = picker.getSelectedDate()
        val time: Triple<Int, Int, Int> = picker.getSelectedTime()

        val startDate: Triple<Int, Int, Int>? = picker.getSelectedStartDate()
        val startTime: Triple<Int, Int, Int>? = picker.getSelectedStartTime()

        val endDate: Triple<Int, Int, Int>? = picker.getSelectedEndDate()
        val endTime: Triple<Int, Int, Int>? = picker.getSelectedEndTime()

        assertEquals(2013, date.first)
        assertEquals(Calendar.FEBRUARY, date.second)
        assertEquals(1, date.third)

        assertEquals(INVALID, time.first)
        assertEquals(INVALID, time.second)
        assertEquals(INVALID, time.third)

        assertNull(startDate)
        assertNull(startTime)

        assertNull(endDate)
        assertNull(endTime)
    }

    @Test
    fun `set selected month - dialog created, picker's values are valid`() {
        dialog = DateTimeMaterialDialogBuilder(context)
            .selectedDate(month = Calendar.FEBRUARY)
            .title("Choose date time")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build() as DateTimeMaterialDialog

        val picker: Picker = dialog.dtPicker

        val date: Triple<Int, Int, Int> = picker.getSelectedDate()
        val time: Triple<Int, Int, Int> = picker.getSelectedTime()

        val startDate: Triple<Int, Int, Int>? = picker.getSelectedStartDate()
        val startTime: Triple<Int, Int, Int>? = picker.getSelectedStartTime()

        val endDate: Triple<Int, Int, Int>? = picker.getSelectedEndDate()
        val endTime: Triple<Int, Int, Int>? = picker.getSelectedEndTime()

        assertEquals(INVALID, date.first)
        assertEquals(Calendar.FEBRUARY, date.second)
        assertEquals(INVALID, date.third)

        assertEquals(INVALID, time.first)
        assertEquals(INVALID, time.second)
        assertEquals(INVALID, time.third)

        assertNull(startDate)
        assertNull(startTime)

        assertNull(endDate)
        assertNull(endTime)
    }

    @Test
    fun `set selected time - dialog created, picker's values are valid`() {
        dialog = DateTimeMaterialDialogBuilder(context)
            .selectedTime(12, 0, 0)
            .title("Choose date time")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build() as DateTimeMaterialDialog

        val picker: Picker = dialog.dtPicker

        val date: Triple<Int, Int, Int> = picker.getSelectedDate()
        val time: Triple<Int, Int, Int> = picker.getSelectedTime()

        val startDate: Triple<Int, Int, Int>? = picker.getSelectedStartDate()
        val startTime: Triple<Int, Int, Int>? = picker.getSelectedStartTime()

        val endDate: Triple<Int, Int, Int>? = picker.getSelectedEndDate()
        val endTime: Triple<Int, Int, Int>? = picker.getSelectedEndTime()

        assertEquals(INVALID, date.first)
        assertEquals(INVALID, date.second)
        assertEquals(INVALID, date.third)

        assertEquals(12, time.first)
        assertEquals(0, time.second)
        assertEquals(0, time.third)

        assertNull(startDate)
        assertNull(startTime)

        assertNull(endDate)
        assertNull(endTime)
    }

    @Test
    fun `set selected dt - dialog created, picker's values are valid`() {
        dialog = DateTimeMaterialDialogBuilder(context)
            .selectedDateTime(2013, Calendar.FEBRUARY, 1, 12, 0, 0)
            .title("Choose date time")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build() as DateTimeMaterialDialog

        val picker: Picker = dialog.dtPicker

        val date: Triple<Int, Int, Int> = picker.getSelectedDate()
        val time: Triple<Int, Int, Int> = picker.getSelectedTime()

        val startDate: Triple<Int, Int, Int>? = picker.getSelectedStartDate()
        val startTime: Triple<Int, Int, Int>? = picker.getSelectedStartTime()

        val endDate: Triple<Int, Int, Int>? = picker.getSelectedEndDate()
        val endTime: Triple<Int, Int, Int>? = picker.getSelectedEndTime()

        assertEquals(2013, date.first)
        assertEquals(Calendar.FEBRUARY, date.second)
        assertEquals(1, date.third)

        assertEquals(12, time.first)
        assertEquals(0, time.second)
        assertEquals(0, time.third)

        assertNull(startDate)
        assertNull(startTime)

        assertNull(endDate)
        assertNull(endTime)
    }

    @Test
    fun `range, set selected date - dialog created, picker's values are valid`() {
        dialogRange = DateTimeRangeMaterialDialogBuilder(context)
            .selectedStartDate(2013, Calendar.FEBRUARY, 1)
            .selectedEndDate(2013, Calendar.FEBRUARY, 2)
            .title("Choose date time")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build() as DateTimeRangeMaterialDialog

        val picker: Picker = dialogRange.dtPicker

        val date: Triple<Int, Int, Int> = picker.getSelectedDate()
        val time: Triple<Int, Int, Int> = picker.getSelectedTime()

        val startDate: Triple<Int, Int, Int>? = picker.getSelectedStartDate()
        val startTime: Triple<Int, Int, Int>? = picker.getSelectedStartTime()

        val endDate: Triple<Int, Int, Int>? = picker.getSelectedEndDate()
        val endTime: Triple<Int, Int, Int>? = picker.getSelectedEndTime()

        assertEquals(2013, date.first)
        assertEquals(Calendar.FEBRUARY, date.second)
        assertEquals(1, date.third)

        assertEquals(INVALID, time.first)
        assertEquals(INVALID, time.second)
        assertEquals(INVALID, time.third)


        assertEquals(2013, startDate?.first)
        assertEquals(Calendar.FEBRUARY, startDate?.second)
        assertEquals(1, startDate?.third)

        assertEquals(INVALID, startTime?.first)
        assertEquals(INVALID, startTime?.second)
        assertEquals(INVALID, startTime?.third)


        assertEquals(2013, endDate?.first)
        assertEquals(Calendar.FEBRUARY, endDate?.second)
        assertEquals(2, endDate?.third)

        assertEquals(INVALID, endTime?.first)
        assertEquals(INVALID, endTime?.second)
        assertEquals(INVALID, endTime?.third)
    }

    @Test
    fun `range, set selected month - dialog created, picker's values are valid`() {
        dialogRange = DateTimeRangeMaterialDialogBuilder(context)
            .selectedStartDate(month = Calendar.FEBRUARY)
            .selectedEndDate(month = Calendar.MARCH)
            .title("Choose date time")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build() as DateTimeRangeMaterialDialog

        val picker: Picker = dialogRange.dtPicker

        val date: Triple<Int, Int, Int> = picker.getSelectedDate()
        val time: Triple<Int, Int, Int> = picker.getSelectedTime()

        val startDate: Triple<Int, Int, Int>? = picker.getSelectedStartDate()
        val startTime: Triple<Int, Int, Int>? = picker.getSelectedStartTime()

        val endDate: Triple<Int, Int, Int>? = picker.getSelectedEndDate()
        val endTime: Triple<Int, Int, Int>? = picker.getSelectedEndTime()

        assertEquals(INVALID, date.first)
        assertEquals(Calendar.FEBRUARY, date.second)
        assertEquals(INVALID, date.third)

        assertEquals(INVALID, time.first)
        assertEquals(INVALID, time.second)
        assertEquals(INVALID, time.third)

        assertEquals(INVALID, startDate?.first)
        assertEquals(Calendar.FEBRUARY, startDate?.second)
        assertEquals(INVALID, startDate?.third)

        assertEquals(INVALID, startTime?.first)
        assertEquals(INVALID, startTime?.second)
        assertEquals(INVALID, startTime?.third)

        assertEquals(INVALID, endDate?.first)
        assertEquals(Calendar.MARCH, endDate?.second)
        assertEquals(INVALID, endDate?.third)

        assertEquals(INVALID, endTime?.first)
        assertEquals(INVALID, endTime?.second)
        assertEquals(INVALID, endTime?.third)
    }

    @Test
    fun `range, set selected time - dialog created, picker's values are valid`() {
        dialogRange = DateTimeRangeMaterialDialogBuilder(context)
            .selectedStartTime(12, 0, 0)
            .selectedEndTime(13, 0, 0)
            .title("Choose date time")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build() as DateTimeRangeMaterialDialog

        val picker: Picker = dialogRange.dtPicker

        val date: Triple<Int, Int, Int> = picker.getSelectedDate()
        val time: Triple<Int, Int, Int> = picker.getSelectedTime()

        val startDate: Triple<Int, Int, Int>? = picker.getSelectedStartDate()
        val startTime: Triple<Int, Int, Int>? = picker.getSelectedStartTime()

        val endDate: Triple<Int, Int, Int>? = picker.getSelectedEndDate()
        val endTime: Triple<Int, Int, Int>? = picker.getSelectedEndTime()

        assertEquals(INVALID, date.first)
        assertEquals(INVALID, date.second)
        assertEquals(INVALID, date.third)

        assertEquals(12, time.first)
        assertEquals(0, time.second)
        assertEquals(0, time.third)


        assertEquals(INVALID, startDate?.first)
        assertEquals(INVALID, startDate?.second)
        assertEquals(INVALID, startDate?.third)

        assertEquals(12, startTime?.first)
        assertEquals(0, startTime?.second)
        assertEquals(0, startTime?.third)


        assertEquals(INVALID, endDate?.first)
        assertEquals(INVALID, endDate?.second)
        assertEquals(INVALID, endDate?.third)

        assertEquals(13, endTime?.first)
        assertEquals(0, endTime?.second)
        assertEquals(0, endTime?.third)
    }

    @Test
    fun `range, set selected dt - dialog created, picker's values are valid`() {
        dialogRange = DateTimeRangeMaterialDialogBuilder(context)
            .selectedStartDateTime(2013, Calendar.FEBRUARY, 1, 12, 0, 0)
            .selectedEndDateTime(2013, Calendar.FEBRUARY, 2, 13, 0, 0)
            .title("Choose date time")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build() as DateTimeRangeMaterialDialog

        val picker: Picker = dialogRange.dtPicker

        val date: Triple<Int, Int, Int> = picker.getSelectedDate()
        val time: Triple<Int, Int, Int> = picker.getSelectedTime()

        val startDate: Triple<Int, Int, Int>? = picker.getSelectedStartDate()
        val startTime: Triple<Int, Int, Int>? = picker.getSelectedStartTime()

        val endDate: Triple<Int, Int, Int>? = picker.getSelectedEndDate()
        val endTime: Triple<Int, Int, Int>? = picker.getSelectedEndTime()

        assertEquals(2013, date.first)
        assertEquals(Calendar.FEBRUARY, date.second)
        assertEquals(1, date.third)

        assertEquals(12, time.first)
        assertEquals(0, time.second)
        assertEquals(0, time.third)


        assertEquals(2013, startDate?.first)
        assertEquals(Calendar.FEBRUARY, startDate?.second)
        assertEquals(1, startDate?.third)

        assertEquals(12, startTime?.first)
        assertEquals(0, startTime?.second)
        assertEquals(0, startTime?.third)


        assertEquals(2013, endDate?.first)
        assertEquals(Calendar.FEBRUARY, endDate?.second)
        assertEquals(2, endDate?.third)

        assertEquals(13, endTime?.first)
        assertEquals(0, endTime?.second)
        assertEquals(0, endTime?.third)
    }
}