object Versions {
    const val kotlin = "1.5.31"
    const val junit = "4.13"
    const val robolectric = "4.5.1"

    object olekdia {
        const val common = "0.6.0"
        const val common_android = "3.7.1"
        const val materialdialog_core = "3.7.2"
        const val date_time_picker = "0.7.1"
    }

    object sdk {
        const val min = 14
        const val target = 30
        const val compile = 30
    }
    const val buildTools = "30.0.3"

    object androidx {
        const val annotations = "1.2.0"
        const val appcompat = "1.3.1"
        const val material = "1.4.0"
        const val fragment = "1.3.6"

        const val test_core = "1.4.0"
        const val test_runner = "1.4.0"
        const val test_rules = "1.4.0"

        const val fragment_testing = "1.3.6"
    }

    const val android_gradle = "7.0.3"
}


object Libs {
    val junit = "junit:junit:${Versions.junit}"
    val robolectric = "org.robolectric:robolectric:${Versions.robolectric}"

    object olekdia {
        private val common_prefix = "com.olekdia:multiplatform-common"
        val common = "$common_prefix:${Versions.olekdia.common}"
        val common_jvm = "$common_prefix-jvm:${Versions.olekdia.common}"
        val common_js = "$common_prefix-js:${Versions.olekdia.common}"
        val common_native = "$common_prefix-native:${Versions.olekdia.common}"
        val common_android = "com.olekdia:android-common:${Versions.olekdia.common_android}"
        val materialdialog_core = "com.olekdia.material-dialog:core:${Versions.olekdia.materialdialog_core}"
        val date_time_picker = "com.olekdia:date-time-picker:${Versions.olekdia.date_time_picker}"
    }

    object androidx {
        val annotations = "androidx.annotation:annotation:${Versions.androidx.annotations}"
        val appcompat = "androidx.appcompat:appcompat:${Versions.androidx.appcompat}"
        val material = "com.google.android.material:material:${Versions.androidx.material}"
        val fragment = "androidx.fragment:fragment:${Versions.androidx.fragment}"

        val test_core = "androidx.test:core:${Versions.androidx.test_core}"
        val test_runner = "androidx.test:runner:${Versions.androidx.test_runner}"
        val test_rules = "androidx.test:rules:${Versions.androidx.test_rules}"

        val fragment_testing = "androidx.fragment:fragment-testing:${Versions.androidx.fragment_testing}"
    }

    object plugin {
        val kotlin_gradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
        val android_gradle = "com.android.tools.build:gradle:${Versions.android_gradle}"
    }
}
