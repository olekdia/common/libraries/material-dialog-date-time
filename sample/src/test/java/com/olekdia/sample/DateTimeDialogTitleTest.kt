package com.olekdia.sample

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.widget.TextView
import androidx.test.core.app.ActivityScenario
import androidx.test.platform.app.InstrumentationRegistry
import com.olekdia.datetimepicker.common.PickerIndex.Companion.DAY
import com.olekdia.datetimepicker.common.PickerIndex.Companion.HOUR
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MINUTE
import com.olekdia.datetimepicker.common.PickerIndex.Companion.MONTH
import com.olekdia.datetimepicker.common.PickerIndex.Companion.SECOND
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import com.olekdia.datetimepicker.common.RangePart.Companion.END
import com.olekdia.datetimepicker.common.RangePart.Companion.START
import com.olekdia.mddt.DateTimeMaterialDialog
import com.olekdia.mddt.DateTimeRangeMaterialDialog
import com.olekdia.sample.DtPickerDialogForTesting.Companion.CURR_ITEM_KEY
import com.olekdia.sample.DtPickerDialogForTesting.Companion.DATE_TITLE_KEY
import com.olekdia.sample.DtPickerDialogForTesting.Companion.MAIN_TITLE_KEY
import com.olekdia.sample.DtPickerDialogForTesting.Companion.TIME_TITLE_KEY
import com.olekdia.sample.DtRangePickerDialogForTesting.Companion.CURR_RANGE_PART_KEY
import com.olekdia.sample.DtRangePickerDialogForTesting.Companion.END_DATE_TITLE_KEY
import com.olekdia.sample.DtRangePickerDialogForTesting.Companion.END_TIME_TITLE_KEY
import com.olekdia.sample.DtRangePickerDialogForTesting.Companion.START_DATE_TITLE_KEY
import com.olekdia.sample.DtRangePickerDialogForTesting.Companion.START_TIME_TITLE_KEY
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE, sdk = [Build.VERSION_CODES.KITKAT])
class DateTimeDialogTitleTest {

    private lateinit var context: Context
    private lateinit var scenario: ActivityScenario<SampleActivity>

    private val title = "title"

    private val dateTitle = "date title"
    private val timeTitle = "time title"

    private val startDateTitle = "start date title"
    private val startTimeTitle = "start time title"

    private val endDateTitle = "end date title"
    private val endTimeTitle = "end time title"

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        scenario = ActivityScenario.launch(SampleActivity::class.java)
    }

    //title valid after creation

    //without range

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, year, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putString(DATE_TITLE_KEY, dateTitle)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(dateTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, month, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, MONTH)
            putString(DATE_TITLE_KEY, dateTitle)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(dateTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, day, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, DAY)
            putString(DATE_TITLE_KEY, dateTitle)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(dateTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, hour, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, HOUR)
            putString(DATE_TITLE_KEY, dateTitle)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(timeTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, minute, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, MINUTE)
            putString(DATE_TITLE_KEY, dateTitle)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(timeTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, second, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, SECOND)
            putString(DATE_TITLE_KEY, dateTitle)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(timeTitle, dialogBefore.titleLabel?.text)
        }
    }

    //with range start

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, year, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putInt(CURR_RANGE_PART_KEY, START)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(startDateTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, month, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, MONTH)
            putInt(CURR_RANGE_PART_KEY, START)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(startDateTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, day, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, DAY)
            putInt(CURR_RANGE_PART_KEY, START)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(startDateTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, hour, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, HOUR)
            putInt(CURR_RANGE_PART_KEY, START)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(startTimeTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, minute, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, MINUTE)
            putInt(CURR_RANGE_PART_KEY, START)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(startTimeTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, second, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, SECOND)
            putInt(CURR_RANGE_PART_KEY, START)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(startTimeTitle, dialogBefore.titleLabel?.text)
        }
    }

    //with range end

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, end, year, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putInt(CURR_RANGE_PART_KEY, END)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(endDateTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, end, month, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, MONTH)
            putInt(CURR_RANGE_PART_KEY, END)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(endDateTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, end, day, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, DAY)
            putInt(CURR_RANGE_PART_KEY, END)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(endDateTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, end, hour, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, HOUR)
            putInt(CURR_RANGE_PART_KEY, END)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(endTimeTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, end, minute, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, MINUTE)
            putInt(CURR_RANGE_PART_KEY, END)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(endTimeTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, end, second, without main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, SECOND)
            putInt(CURR_RANGE_PART_KEY, END)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(endTimeTitle, dialogBefore.titleLabel?.text)
        }
    }

    //title valid after changing of header selection

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, year, without main title, pick header item - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putString(DATE_TITLE_KEY, dateTitle)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(dateTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.start_month)
                .performClick()
            assertEquals(dateTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.start_day)
                .performClick()
            assertEquals(dateTitle, dialogBefore.titleLabel?.text)


            dialogBefore.view
                .findViewById<TextView>(R.id.start_hours)
                .performClick()
            assertEquals(timeTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.start_minutes)
                .performClick()
            assertEquals(timeTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.start_seconds)
                .performClick()
            assertEquals(timeTitle, dialogBefore.titleLabel?.text)


            dialogBefore.view
                .findViewById<TextView>(R.id.start_year)
                .performClick()
            assertEquals(dateTitle, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, year, without main title, pick header item - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putInt(CURR_RANGE_PART_KEY, START)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(startDateTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.start_month)
                .performClick()
            assertEquals(startDateTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.start_day)
                .performClick()
            assertEquals(startDateTitle, dialogBefore.titleLabel?.text)


            dialogBefore.view
                .findViewById<TextView>(R.id.start_hours)
                .performClick()
            assertEquals(startTimeTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.start_minutes)
                .performClick()
            assertEquals(startTimeTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.start_seconds)
                .performClick()
            assertEquals(startTimeTitle, dialogBefore.titleLabel?.text)


            dialogBefore.view
                .findViewById<TextView>(R.id.end_year)
                .performClick()
            assertEquals(endDateTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.end_month)
                .performClick()
            assertEquals(endDateTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.end_day)
                .performClick()
            assertEquals(endDateTitle, dialogBefore.titleLabel?.text)


            dialogBefore.view
                .findViewById<TextView>(R.id.end_hours)
                .performClick()
            assertEquals(endTimeTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.end_minutes)
                .performClick()
            assertEquals(endTimeTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.end_seconds)
                .performClick()
            assertEquals(endTimeTitle, dialogBefore.titleLabel?.text)


            dialogBefore.view
                .findViewById<TextView>(R.id.start_year)
                .performClick()
            assertEquals(startDateTitle, dialogBefore.titleLabel?.text)
        }
    }

    //rotate

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, year, without main title, rotate - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putString(DATE_TITLE_KEY, dateTitle)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog
        lateinit var dialogAfter: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(dateTitle, dialogBefore.titleLabel?.text)
        }

        scenario.recreate()

        scenario.onActivity { activity ->
            val fragment: DtPickerDialogForTesting = activity
                .supportFragmentManager
                .findFragmentByTag(DtPickerDialogForTesting.TAG) as DtPickerDialogForTesting

            dialogAfter = fragment.dialog

            assertNotEquals(dialogBefore, dialogAfter)

            assertEquals(dateTitle, dialogAfter.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, year, without main title, pick header item, rotate - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putString(DATE_TITLE_KEY, dateTitle)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog
        lateinit var dialogAfter: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(dateTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.start_hours)
                .performClick()
            assertEquals(timeTitle, dialogBefore.titleLabel?.text)
        }

        scenario.recreate()

        scenario.onActivity { activity ->
            val fragment: DtPickerDialogForTesting = activity
                .supportFragmentManager
                .findFragmentByTag(DtPickerDialogForTesting.TAG) as DtPickerDialogForTesting

            dialogAfter = fragment.dialog

            assertNotEquals(dialogBefore, dialogAfter)

            assertEquals(timeTitle, dialogAfter.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, year, without main title, rotate - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putInt(CURR_RANGE_PART_KEY, START)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog
        lateinit var dialogAfter: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(startDateTitle, dialogBefore.titleLabel?.text)
        }

        scenario.recreate()

        scenario.onActivity { activity ->
            val fragment: DtRangePickerDialogForTesting = activity
                .supportFragmentManager
                .findFragmentByTag(DtRangePickerDialogForTesting.TAG) as DtRangePickerDialogForTesting

            dialogAfter = fragment.dialog

            assertNotEquals(dialogBefore, dialogAfter)

            assertEquals(startDateTitle, dialogAfter.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, year, without main title, pick header item, rotate - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putInt(CURR_RANGE_PART_KEY, START)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog
        lateinit var dialogAfter: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(startDateTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.end_hours)
                .performClick()
            assertEquals(endTimeTitle, dialogBefore.titleLabel?.text)
        }

        scenario.recreate()

        scenario.onActivity { activity ->
            val fragment: DtRangePickerDialogForTesting = activity
                .supportFragmentManager
                .findFragmentByTag(DtRangePickerDialogForTesting.TAG) as DtRangePickerDialogForTesting

            dialogAfter = fragment.dialog

            assertNotEquals(dialogBefore, dialogAfter)

            assertEquals(endTimeTitle, dialogAfter.titleLabel?.text)
        }
    }


    //main title set

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, year, with main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putString(MAIN_TITLE_KEY, title)
            putString(DATE_TITLE_KEY, dateTitle)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(title, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, year, with main title, pick header item - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putString(MAIN_TITLE_KEY, title)
            putString(DATE_TITLE_KEY, dateTitle)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(title, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.start_hours)
                .performClick()
            assertEquals(title, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, year, with main title, rotate - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putString(MAIN_TITLE_KEY, title)
            putString(DATE_TITLE_KEY, dateTitle)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog
        lateinit var dialogAfter: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(title, dialogBefore.titleLabel?.text)
        }

        scenario.recreate()

        scenario.onActivity { activity ->
            val fragment: DtPickerDialogForTesting = activity
                .supportFragmentManager
                .findFragmentByTag(DtPickerDialogForTesting.TAG) as DtPickerDialogForTesting

            dialogAfter = fragment.dialog

            assertNotEquals(dialogBefore, dialogAfter)

            assertEquals(title, dialogAfter.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, year, with main title, pick header item, rotate - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putString(MAIN_TITLE_KEY, title)
            putString(DATE_TITLE_KEY, dateTitle)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog
        lateinit var dialogAfter: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(title, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.start_hours)
                .performClick()
            assertEquals(title, dialogBefore.titleLabel?.text)
        }

        scenario.recreate()

        scenario.onActivity { activity ->
            val fragment: DtPickerDialogForTesting = activity
                .supportFragmentManager
                .findFragmentByTag(DtPickerDialogForTesting.TAG) as DtPickerDialogForTesting

            dialogAfter = fragment.dialog

            assertNotEquals(dialogBefore, dialogAfter)

            assertEquals(title, dialogAfter.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, year, with main title - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putInt(CURR_RANGE_PART_KEY, START)
            putString(MAIN_TITLE_KEY, title)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(title, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, year, with main title, pick header item - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putInt(CURR_RANGE_PART_KEY, START)
            putString(MAIN_TITLE_KEY, title)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(title, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.end_hours)
                .performClick()
            assertEquals(title, dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, year, with main title, rotate - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putInt(CURR_RANGE_PART_KEY, START)
            putString(MAIN_TITLE_KEY, title)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog
        lateinit var dialogAfter: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(title, dialogBefore.titleLabel?.text)
        }

        scenario.recreate()

        scenario.onActivity { activity ->
            val fragment: DtRangePickerDialogForTesting = activity
                .supportFragmentManager
                .findFragmentByTag(DtRangePickerDialogForTesting.TAG) as DtRangePickerDialogForTesting

            dialogAfter = fragment.dialog

            assertNotEquals(dialogBefore, dialogAfter)

            assertEquals(title, dialogAfter.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, year, with main title, pick header item, rotate - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putInt(CURR_RANGE_PART_KEY, START)
            putString(MAIN_TITLE_KEY, title)
            putString(START_DATE_TITLE_KEY, startDateTitle)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog
        lateinit var dialogAfter: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(title, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.end_hours)
                .performClick()
            assertEquals(title, dialogBefore.titleLabel?.text)
        }

        scenario.recreate()

        scenario.onActivity { activity ->
            val fragment: DtRangePickerDialogForTesting = activity
                .supportFragmentManager
                .findFragmentByTag(DtRangePickerDialogForTesting.TAG) as DtRangePickerDialogForTesting

            dialogAfter = fragment.dialog

            assertNotEquals(dialogBefore, dialogAfter)

            assertEquals(title, dialogAfter.titleLabel?.text)
        }
    }

    //some of title don't set

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, year, without main title, dateTitle don't set - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals("", dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, hour, without main title, dateTitle don't set, pick header item - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, HOUR)
            putString(TIME_TITLE_KEY, timeTitle)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(timeTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.start_year)
                .performClick()
            assertEquals("", dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, year, without main title, startDateTitle don't set - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putInt(CURR_RANGE_PART_KEY, START)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals("", dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, end, hour, without main title, startDateTitle don't set, pick header item - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, HOUR)
            putInt(CURR_RANGE_PART_KEY, END)
            putString(START_TIME_TITLE_KEY, startTimeTitle)
            putString(END_DATE_TITLE_KEY, endDateTitle)
            putString(END_TIME_TITLE_KEY, endTimeTitle)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals(endTimeTitle, dialogBefore.titleLabel?.text)

            dialogBefore.view
                .findViewById<TextView>(R.id.start_year)
                .performClick()
            assertEquals("", dialogBefore.titleLabel?.text)
        }
    }

    //all titles don't set

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, without range, year, all titles don't set - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
        }
        lateinit var dialogBefore: DateTimeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtPickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals("", dialogBefore.titleLabel?.text)
        }
    }

    @Test
    @LooperMode(LooperMode.Mode.LEGACY)
    fun `create dialog, with range, start, year, all titles don't set - title is valid`() {
        val args = Bundle().apply {
            putInt(CURR_ITEM_KEY, YEAR)
            putInt(CURR_RANGE_PART_KEY, START)
        }
        lateinit var dialogBefore: DateTimeRangeMaterialDialog

        scenario.onActivity { activity ->
            val fragment = DtRangePickerDialogForTesting()
            fragment.show(activity.supportFragmentManager, args, DtPickerDialogForTesting.TAG)
            dialogBefore = fragment.dialog

            assertEquals("", dialogBefore.titleLabel?.text)
        }
    }
}