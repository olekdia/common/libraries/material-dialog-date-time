package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.mddt.DateTimeRangeMaterialDialog
import com.olekdia.mddt.DateTimeRangeMaterialDialogBuilder

class YearRangePickerDialog :
    BaseSampleDialog(),
    DateTimeRangeMaterialDialog.DtCallback {

    private lateinit var dialog: DateTimeRangeMaterialDialog

    override fun getDialogTag(): String = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle: Boolean =
            arguments?.getBoolean(BOTTOM_SHEET_STYLE, false) ?: false

        dialog = DateTimeRangeMaterialDialogBuilder(requireContext())
            .selectedStartDate(2013)
            .selectedEndDate(2014)
            .alwaysCallDtCallback()
            .dtCallback(this)
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Choose year")
            .positiveText("Ok")
            .negativeText("Cancel")
            .callback(this)
            .build()
            .also {
                it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            } as DateTimeRangeMaterialDialog

        return dialog
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        startYear: Int, startMonth: Int, startDay: Int,
        startHour: Int, startMinute: Int, startSecond: Int,
        endYear: Int, endMonth: Int, endDay: Int,
        endHour: Int, endMinute: Int, endSecond: Int
    ) {
        val text: String = dateToStr(startYear) +
            "\n" +
            dateToStr(endYear)

        ToastHelper.showToast(
            requireContext(),
            "Selected date: \n $text"
        )
    }

    private fun dateToStr(year: Int): String =
        "$year"

    companion object {
        const val TAG = "YEAR_RANGE_PICKER_DLG"
    }
}