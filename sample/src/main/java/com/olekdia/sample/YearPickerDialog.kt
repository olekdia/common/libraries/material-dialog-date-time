package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.mddt.DateTimeMaterialDialog
import com.olekdia.mddt.DateTimeMaterialDialogBuilder

class YearPickerDialog :
    BaseSampleDialog(),
    DateTimeMaterialDialog.DtCallback {

    private lateinit var dialog: DateTimeMaterialDialog

    override fun getDialogTag(): String = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle: Boolean =
            arguments?.getBoolean(BOTTOM_SHEET_STYLE, false) ?: false

        dialog = DateTimeMaterialDialogBuilder(requireContext())
            .selectedDate(2013)
            .alwaysCallDtCallback()
            .dtCallback(this)
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Choose year")
            .positiveText("Ok")
            .negativeText("Cancel")
            .callback(this)
            .build()
            .also {
                it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            } as DateTimeMaterialDialog

        return dialog
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        year: Int, month: Int, day: Int,
        hour: Int, minute: Int, second: Int
    ) {
        val text: String = dateToStr(year)

        ToastHelper.showToast(
            requireContext(),
            "Selected date: $text"
        )
    }

    private fun dateToStr(year: Int): String =
        "$year"

    companion object {
        const val TAG = "YEAR_PICKER_DLG"
    }
}