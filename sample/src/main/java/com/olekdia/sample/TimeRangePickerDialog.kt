package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.mddt.DateTimeRangeMaterialDialog
import com.olekdia.mddt.DateTimeRangeMaterialDialogBuilder

class TimeRangePickerDialog :
    BaseSampleDialog(),
    DateTimeRangeMaterialDialog.DtCallback {

    private lateinit var dialog: DateTimeRangeMaterialDialog

    override fun getDialogTag(): String = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle: Boolean =
            arguments?.getBoolean(BOTTOM_SHEET_STYLE, false) ?: false

        dialog = DateTimeRangeMaterialDialogBuilder(requireContext())
            .selectedStartTime(12, 0, 0)
            .selectedEndTime(13, 0, 0)
            .alwaysCallDtCallback()
            .dtCallback(this)
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Choose date time")
            .positiveText("Ok")
            .negativeText("Cancel")
            .callback(this)
            .build()
            .also {
                it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            } as DateTimeRangeMaterialDialog

        return dialog
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        startYear: Int, startMonth: Int, startDay: Int,
        startHour: Int, startMinute: Int, startSecond: Int,
        endYear: Int, endMonth: Int, endDay: Int,
        endHour: Int, endMinute: Int, endSecond: Int
    ) {
        val text: String = timeToStr(startHour, startMinute, startSecond) +
            "\n" +
            timeToStr(endHour, endMinute, endSecond)

        ToastHelper.showToast(
            requireContext(),
            "Selected time: \n $text"
        )
    }

    private fun timeToStr(hour: Int, minute: Int, second: Int): String =
        "$hour:$minute:$second"

    companion object {
        const val TAG = "TIME_RANGE_PICKER_DLG"
    }
}