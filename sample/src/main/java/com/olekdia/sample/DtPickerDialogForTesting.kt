package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import androidx.fragment.app.FragmentManager
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.mddt.DateTimeMaterialDialog
import com.olekdia.mddt.DateTimeMaterialDialogBuilder
import java.util.*

class DtPickerDialogForTesting :
    BaseSampleDialog(),
    DateTimeMaterialDialog.DtCallback {

    internal lateinit var dialog: DateTimeMaterialDialog

    override fun getDialogTag(): String = TAG

    fun show(fm: FragmentManager, args: Bundle?, tag: String?) {
        arguments = args
        show(fm, tag)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val currItem: Int = arguments?.getInt(CURR_ITEM_KEY) ?: YEAR
        val mainTitle: String? = arguments?.getString(MAIN_TITLE_KEY, null)
        val dateTitle: String? = arguments?.getString(DATE_TITLE_KEY, null)
        val timeTitle: String? = arguments?.getString(TIME_TITLE_KEY, null)

        dialog = DateTimeMaterialDialogBuilder(requireContext())
            .currentItem(currItem)
            .selectedDateTime(2013, Calendar.SEPTEMBER, 1, 12, 0, 0)
            .also {
                dateTitle?.let { title ->
                    it.dateTitle(title)
                }

                timeTitle?.let { title ->
                    it.timeTitle(title)
                }
            }
            .alwaysCallDtCallback()
            .dtCallback(this)
            .bottomSheet(
                bottomSheet = false,
                forceExpand = false
            )
            .also {
                mainTitle?.let { mainTitle ->
                    it.title(mainTitle)
                }
            }
            .positiveText("Ok")
            .negativeText("Cancel")
            .callback(this)
            .build()
            .also {
                it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            } as DateTimeMaterialDialog

        return dialog
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        year: Int, month: Int, day: Int,
        hour: Int, minute: Int, second: Int
    ) {
    }

    companion object {
        const val TAG = "DT_PICKER_TEST_DLG"

        const val CURR_ITEM_KEY = "CURR_ITEM_KEY"

        const val MAIN_TITLE_KEY = "MAIN_TITLE_KEY"
        const val DATE_TITLE_KEY = "DATE_TITLE_KEY"
        const val TIME_TITLE_KEY = "TIME_TITLE_KEY"
    }
}