package com.olekdia.sample

import android.content.Context
import android.widget.Toast

object ToastHelper {

    private var toast: Toast? = null

    fun showToast(toast: Toast) {
        cancelToast()
        this.toast = toast.also { it.show() }
    }

    fun cancelToast() {
        toast?.cancel()
        toast = null
    }

    fun showToast(context: Context, text: CharSequence) {
        cancelToast()
        toast = Toast.makeText(context, text, Toast.LENGTH_SHORT).also {
            it.show()
        }
    }
}