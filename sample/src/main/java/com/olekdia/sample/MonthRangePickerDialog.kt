package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.mddt.DateTimeRangeMaterialDialog
import com.olekdia.mddt.DateTimeRangeMaterialDialogBuilder
import java.util.*

class MonthRangePickerDialog :
    BaseSampleDialog(),
    DateTimeRangeMaterialDialog.DtCallback {

    private lateinit var dialog: DateTimeRangeMaterialDialog

    override fun getDialogTag(): String = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle: Boolean =
            arguments?.getBoolean(BOTTOM_SHEET_STYLE, false) ?: false

        dialog = DateTimeRangeMaterialDialogBuilder(requireContext())
            .selectedStartDate(2013, Calendar.JANUARY)
            .selectedEndDate(2013, Calendar.FEBRUARY)
            .alwaysCallDtCallback()
            .dtCallback(this)
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Choose month")
            .positiveText("Ok")
            .negativeText("Cancel")
            .callback(this)
            .build()
            .also {
                it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            } as DateTimeRangeMaterialDialog

        return dialog
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        startYear: Int, startMonth: Int, startDay: Int,
        startHour: Int, startMinute: Int, startSecond: Int,
        endYear: Int, endMonth: Int, endDay: Int,
        endHour: Int, endMinute: Int, endSecond: Int
    ) {
        val text: String = dateToStr(startYear, startMonth) +
            "\n" +
            dateToStr(endYear, endMonth)

        ToastHelper.showToast(
            requireContext(),
            "Selected date: \n $text"
        )
    }

    private fun dateToStr(year: Int, month: Int): String =
        "$year $month"

    companion object {
        const val TAG = "MONTH_RANGE_PICKER_DLG"
    }
}