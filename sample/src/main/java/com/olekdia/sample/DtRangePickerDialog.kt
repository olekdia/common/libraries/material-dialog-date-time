package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.mddt.DateTimeRangeMaterialDialog
import com.olekdia.mddt.DateTimeRangeMaterialDialogBuilder
import java.util.*

class DtRangePickerDialog :
    BaseSampleDialog(),
    DateTimeRangeMaterialDialog.DtCallback {

    private lateinit var dialog: DateTimeRangeMaterialDialog

    override fun getDialogTag(): String = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle: Boolean =
            arguments?.getBoolean(BOTTOM_SHEET_STYLE, false) ?: false

        dialog = DateTimeRangeMaterialDialogBuilder(requireContext())
            .selectedStartDateTime(2013, Calendar.SEPTEMBER, 1, 12, 0, 0)
            .selectedEndDateTime(2013, Calendar.SEPTEMBER, 2, 12, 0, 0)
            .startDateTitle("Choose start date")
            .startTimeTitle("Choose start time")
            .endDateTitle("Choose end date")
            .endTimeTitle("Choose end time")
            .alwaysCallDtCallback()
            .dtCallback(this)
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
//            .title("Choose date time")
            .positiveText("Ok")
            .negativeText("Cancel")
            .callback(this)
            .build()
            .also {
                it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            } as DateTimeRangeMaterialDialog

        return dialog
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        startYear: Int, startMonth: Int, startDay: Int,
        startHour: Int, startMinute: Int, startSecond: Int,
        endYear: Int, endMonth: Int, endDay: Int,
        endHour: Int, endMinute: Int, endSecond: Int
    ) {
        val text: String =
            dateToStr(startYear, startMonth, startDay) +
                " - " +
                timeToStr(startHour, startMinute, startSecond) +
                "\n" +
                dateToStr(endYear, endMonth, endDay) +
                " - " +
                timeToStr(endHour, endMinute, endSecond)

        ToastHelper.showToast(
            requireContext(),
            "Selected Dt: \n $text"
        )
    }

    private fun dateToStr(year: Int, month: Int, day: Int): String =
        "$year $month $day"

    private fun timeToStr(hour: Int, minute: Int, second: Int): String =
        "$hour:$minute:$second"

    companion object {
        const val TAG = "DT_RANGE_PICKER_DLG"
    }
}