package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.mddt.DateTimeRangeMaterialDialog
import com.olekdia.mddt.DateTimeRangeMaterialDialogBuilder
import java.util.*

class DateRangePickerDialog :
    BaseSampleDialog(),
    DateTimeRangeMaterialDialog.DtCallback {

    private lateinit var dialog: DateTimeRangeMaterialDialog

    override fun getDialogTag(): String = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle: Boolean =
            arguments?.getBoolean(BOTTOM_SHEET_STYLE, false) ?: false

        dialog = DateTimeRangeMaterialDialogBuilder(requireContext())
            .selectedStartDate(2013, Calendar.JANUARY, 1)
            .selectedEndDate(2013, Calendar.JANUARY, 2)
            .alwaysCallDtCallback()
            .dtCallback(this)
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Choose date time")
            .positiveText("Ok")
            .negativeText("Cancel")
            .callback(this)
            .build()
            .also {
                it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            } as DateTimeRangeMaterialDialog

        return dialog
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        startYear: Int, startMonth: Int, startDay: Int,
        startHour: Int, startMinute: Int, startSecond: Int,
        endYear: Int, endMonth: Int, endDay: Int,
        endHour: Int, endMinute: Int, endSecond: Int
    ) {
        val text: String = dateToStr(startYear, startMonth, startDay) +
            "\n" +
            dateToStr(endYear, endMonth, endDay)

        ToastHelper.showToast(
            requireContext(),
            "Selected date: \n $text"
        )
    }

    private fun dateToStr(year: Int, month: Int, day: Int): String =
        "$year $month $day"

    companion object {
        const val TAG = "DATE_RANGE_PICKER_DLG"
    }
}