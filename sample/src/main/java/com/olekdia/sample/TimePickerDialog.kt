package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.mddt.DateTimeMaterialDialog
import com.olekdia.mddt.DateTimeMaterialDialogBuilder

class TimePickerDialog :
    BaseSampleDialog(),
    DateTimeMaterialDialog.DtCallback {

    private lateinit var dialog: DateTimeMaterialDialog

    override fun getDialogTag(): String = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle: Boolean =
            arguments?.getBoolean(BOTTOM_SHEET_STYLE, false) ?: false

        dialog = DateTimeMaterialDialogBuilder(requireContext())
            .selectedTime(12, 0, 0)
            .alwaysCallDtCallback()
            .dtCallback(this)
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Choose date time")
            .positiveText("Ok")
            .negativeText("Cancel")
            .callback(this)
            .build()
            .also {
                it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            } as DateTimeMaterialDialog

        return dialog
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        year: Int, month: Int, day: Int,
        hour: Int, minute: Int, second: Int
    ) {
        val text: String = timeToStr(hour, minute, second)

        ToastHelper.showToast(
            requireContext(),
            "Selected time: $text"
        )
    }

    private fun timeToStr(hour: Int, minute: Int, second: Int): String =
        "$hour:$minute:$second"

    companion object {
        const val TAG = "TIME_PICKER_DLG"
    }
}