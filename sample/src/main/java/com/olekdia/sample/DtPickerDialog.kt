package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.mddt.DateTimeMaterialDialog
import com.olekdia.mddt.DateTimeMaterialDialogBuilder
import java.util.*

class DtPickerDialog :
    BaseSampleDialog(),
    DateTimeMaterialDialog.DtCallback {

    private lateinit var dialog: DateTimeMaterialDialog

    override fun getDialogTag(): String = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle: Boolean =
            arguments?.getBoolean(BOTTOM_SHEET_STYLE, false) ?: false

        dialog = DateTimeMaterialDialogBuilder(requireContext())
            .selectedDateTime(2013, Calendar.SEPTEMBER, 1, 12, 0, 0)
            .dateTitle("Choose date")
            .timeTitle("Choose time")
            .alwaysCallDtCallback()
            .dtCallback(this)
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
//            .title("Choose date time")
            .positiveText("Ok")
            .negativeText("Cancel")
            .callback(this)
            .build()
            .also {
                it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            } as DateTimeMaterialDialog

        return dialog
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        year: Int, month: Int, day: Int,
        hour: Int, minute: Int, second: Int
    ) {
        val text: String = dateToStr(year, month, day) +
            " - " +
            timeToStr(hour, minute, second)

        ToastHelper.showToast(
            requireContext(),
            "Selected Dt: $text"
        )
    }

    private fun dateToStr(year: Int, month: Int, day: Int): String =
        "$year $month $day"

    private fun timeToStr(hour: Int, minute: Int, second: Int): String =
        "$hour:$minute:$second"

    companion object {
        const val TAG = "DT_PICKER_DLG"
    }
}