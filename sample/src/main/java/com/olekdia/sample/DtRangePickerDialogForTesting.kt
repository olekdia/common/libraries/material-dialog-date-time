package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import androidx.fragment.app.FragmentManager
import com.olekdia.datetimepicker.common.PickerIndex.Companion.YEAR
import com.olekdia.datetimepicker.common.RangePart.Companion.START
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.mddt.DateTimeRangeMaterialDialog
import com.olekdia.mddt.DateTimeRangeMaterialDialogBuilder
import java.util.*

class DtRangePickerDialogForTesting :
    BaseSampleDialog(),
    DateTimeRangeMaterialDialog.DtCallback {

    internal lateinit var dialog: DateTimeRangeMaterialDialog

    override fun getDialogTag(): String = TAG

    fun show(fm: FragmentManager, args: Bundle?, tag: String?) {
        arguments = args
        show(fm, tag)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val currItem: Int = arguments?.getInt(CURR_ITEM_KEY) ?: YEAR
        val currRangePart: Int = arguments?.getInt(CURR_RANGE_PART_KEY) ?: START
        val mainTitle: String? = arguments?.getString(MAIN_TITLE_KEY, null)
        val startDateTitle: String? = arguments?.getString(START_DATE_TITLE_KEY, null)
        val startTimeTitle: String? = arguments?.getString(START_TIME_TITLE_KEY, null)
        val endDateTitle: String? = arguments?.getString(END_DATE_TITLE_KEY, null)
        val endTimeTitle: String? = arguments?.getString(END_TIME_TITLE_KEY, null)

        dialog = DateTimeRangeMaterialDialogBuilder(requireContext())
            .currentItem(currItem)
            .currentRangePart(currRangePart)
            .selectedStartDateTime(2013, Calendar.SEPTEMBER, 1, 12, 0, 0)
            .selectedEndDateTime(2013, Calendar.SEPTEMBER, 2, 12, 0, 0)
            .also {
                startDateTitle?.let { title ->
                    it.startDateTitle(title)
                }

                startTimeTitle?.let { title ->
                    it.startTimeTitle(title)
                }

                endDateTitle?.let { title ->
                    it.endDateTitle(title)
                }

                endTimeTitle?.let { title ->
                    it.endTimeTitle(title)
                }
            }
            .alwaysCallDtCallback()
            .dtCallback(this)
            .bottomSheet(
                bottomSheet = false,
                forceExpand = false
            )
            .also {
                mainTitle?.let { mainTitle ->
                    it.title(mainTitle)
                }
            }
            .positiveText("Ok")
            .negativeText("Cancel")
            .callback(this)
            .build()
            .also {
                it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            } as DateTimeRangeMaterialDialog

        return dialog
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        startYear: Int, startMonth: Int, startDay: Int,
        startHour: Int, startMinute: Int, startSecond: Int,
        endYear: Int, endMonth: Int, endDay: Int,
        endHour: Int, endMinute: Int, endSecond: Int
    ) {
    }

    companion object {
        const val TAG = "DT_PICKER_TEST_DLG"

        const val CURR_ITEM_KEY = "CURR_ITEM_KEY"
        const val CURR_RANGE_PART_KEY = "CURR_RANGE_PART_KEY"

        const val MAIN_TITLE_KEY = "MAIN_TITLE_KEY"
        const val START_DATE_TITLE_KEY = "START_DATE_TITLE_KEY"
        const val START_TIME_TITLE_KEY = "START_TIME_TITLE_KEY"
        const val END_DATE_TITLE_KEY = "END_DATE_TITLE_KEY"
        const val END_TIME_TITLE_KEY = "END_TIME_TITLE_KEY"
    }
}