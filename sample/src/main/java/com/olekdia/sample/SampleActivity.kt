package com.olekdia.sample

import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity

class SampleActivity : AppCompatActivity() {

    private lateinit var bottomSheetStyle: RadioButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)

        val fm = supportFragmentManager
        val styleGroup = findViewById<RadioGroup>(R.id.dialog_style)

        styleGroup.check(R.id.bottom_sheet_style)

        bottomSheetStyle = findViewById(R.id.bottom_sheet_style)

        findViewById<View>(R.id.date_picker)
            .setOnClickListener { v: View? ->
                DatePickerDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.date_range_picker)
            .setOnClickListener { v: View? ->
                DateRangePickerDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.month_picker)
            .setOnClickListener { v: View? ->
                MonthPickerDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.month_range_picker)
            .setOnClickListener { v: View? ->
                MonthRangePickerDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.year_picker)
            .setOnClickListener { v: View? ->
                YearPickerDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.year_range_picker)
            .setOnClickListener { v: View? ->
                YearRangePickerDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.time_picker)
            .setOnClickListener { v: View? ->
                TimePickerDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.time_range_picker)
            .setOnClickListener { v: View? ->
                TimeRangePickerDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.dt_picker)
            .setOnClickListener { v: View? ->
                DtPickerDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.dt_range_picker)
            .setOnClickListener { v: View? ->
                DtRangePickerDialog()
                    .show(fm, isBottomSheetSelected)
            }
    }

    private val isBottomSheetSelected: Boolean
        get() = bottomSheetStyle.isChecked
}