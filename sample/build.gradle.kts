plugins {
    id("com.android.application")
    kotlin("android")
}

android {
    compileSdk = Versions.sdk.compile
    buildToolsVersion = Versions.buildTools

    defaultConfig {
        minSdk = Versions.sdk.min
        targetSdk = Versions.sdk.target
        applicationId = "com.olekdia.sample"
        versionCode = 1
        versionName = "1.0.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            isShrinkResources = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            isMinifyEnabled = false
            isShrinkResources = false
        }
    }

    testOptions.unitTests.isIncludeAndroidResources = true

    compileOptions {
        encoding = "UTF-8"
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(kotlin("stdlib", Versions.kotlin))
    implementation(Libs.olekdia.common_jvm)
    implementation(Libs.olekdia.common_android)
    implementation(Libs.olekdia.materialdialog_core)
    implementation(Libs.olekdia.date_time_picker)
    implementation(project(":date-time"))

    implementation(Libs.androidx.annotations)
    implementation(Libs.androidx.fragment)
    implementation(Libs.androidx.appcompat)
    implementation(Libs.androidx.material)

    implementation(Libs.androidx.fragment_testing)

    testImplementation(Libs.junit)
    testImplementation(Libs.robolectric)
    testImplementation(Libs.androidx.test_core)
    testImplementation(Libs.androidx.test_runner)
    testImplementation(Libs.androidx.test_rules)
}